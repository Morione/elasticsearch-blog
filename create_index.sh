#!/usr/bin/env bash

# TODO: slovnik synonymov pracovnych pozicii je len ilustracny a obsahuje len niekolko profesii

curl -X PUT http://localhost:9200/jobs \
	-H "Expect:" \
	-H "Content-Type: application/json; charset=utf-8" \
	--data-binary @- << EOF
{
	"settings": {
		"analysis": {
			"filter": {
				"slovak_stop": {
					"type": "stop",
					"stopwords": ["a","aby","aj","ak","ako","ale","alebo","and","ani","áno","asi","až","bez","bude","budem","budeš","budeme","budete","budú","by","bol","bola","boli","bolo","byť","cez","čo","či","ďalší","ďalšia","ďalšie","dnes","do","ho","ešte","for","i","ja","je","jeho","jej","ich","iba","iné","iný","som","si","sme","sú","k","kam","každý","každá","každé","každí","kde","keď","kto","ktorá","ktoré","ktorou","ktorý","ktorí","ku","lebo","len","ma","mať","má","máte","medzi","mi","mna","mne","mnou","musieť","môcť","môj","môže","my","na","nad","nám","náš","naši","nie","nech","než","nič","niektorý","nové","nový","nová","nové","noví","o","od","odo","of","on","ona","ono","oni","ony","po","pod","podľa","pokiaľ","potom","práve","pre","prečo","preto","pretože","prvý","prvá","prvé","prví","pred","predo","pri","pýta","s","sa","so","si","svoje","svoj","svojich","svojím","svojími","ta","tak","takže","táto","teda","te","tě","ten","tento","the","tieto","tým","týmto","tiež","to","toto","toho","tohoto","tom","tomto","tomuto","toto","tu","tú","túto","tvoj","ty","tvojími","už","v","vám","váš","vaše","vo","viac","však","všetok","vy","z","za","zo","že"],
					"ignore_case": true
				},
				"slovak_job_synonym": {
					"type": "synonym",
					"synonyms": [
						"stolár, stolárstvo, stolárske práce",
						"murár, murárske práce",
						"lekár, doktor, chirurg",
						"programátor, developer, vývojár, softvérový inžinier, kóder",
						"učiteľ, profesor",
						"administratívny pracovník, referent",
						"archivár, správca registratúry",
						"sekretárka, office manažérka, asistentka",
						"baník, banský technik",
						"hutnícky inžinier, materiálový inžinier",
						"hutník, metalurg",
						"zlievač, tavič, formovač",
						"back office špecialista, referent back office",
						"broker, maklér",
						"klientský pracovník, teller, klientský poradca, priehradkový pracovník",
						"bankár, osobný bankár, privátny bankár",
						"vzťahový manažér, relationship manager",
						"hasič, záchranár, požiarnik",
						"pracovník bezpečnostnej služby, pracovník SBS, pracovník strážnej služby, strážnik",
						"športovec, futbalista, hokejista, atlét, tenista",
						"technik BOZP, bezpečnostný technik",
						"technik požiarnej ochrany, požiarny technik, požiarnik",
						"väzenský dozorca, príslušník zboru väzenskej a justičnej stráže, bachár",
						"vojak, profesionálny vojak, vojak z povolania"
					],
					"ignore_case": true
				}
			},
			"char_filter": {},
			"tokenizer" : {},
		  	"analyzer": {
		        "slovencina": {
		          	"type": "custom",
		          	"char_filter": ["html_strip"],
		          	"tokenizer": "standard",
		          	"filter": [
		          		"lowercase",
		            	"slovak_stop",
		            	"slovak_job_synonym",
		            	"asciifolding"
		          	]
		        }
		  	}
		}
	},
	"mappings": {
		"properties" : {
			"name" : {
				"type" : "text",
				"copy_to": "summary",
				"analyzer": "slovencina"
			},
			"position" : {
				"type" : "text",
				"copy_to": "summary",
				"analyzer": "slovencina"
			},
			"city" : {
				"type" : "text",
				"copy_to": "summary",
				"analyzer": "slovencina"
			},
			"summary" : {
				"type" : "text",
				"analyzer": "slovencina"
			}
		}
	}
}
EOF